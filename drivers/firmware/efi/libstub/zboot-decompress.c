/* SPDX-License-Identifier: GPL-2.0 */

#include <linux/efi.h>
#include <linux/elf.h>

#include <asm/efi.h>

#include "efistub.h"

extern u32 __aligned(1) payload_size;

static struct {
#ifdef CONFIG_64BIT
	Elf64_Ehdr	ehdr;
	Elf64_Phdr	phdr[5];
#else
	Elf32_Ehdr	ehdr;
	Elf32_Phdr	phdr[5];
#endif
} elf_header;

static bool is_elf;

bool efi_zboot_check_header(unsigned long *alloc_size,
			    unsigned long *entry)
{
	unsigned long min = ULONG_MAX, max = 0;
	bool ret;

	ret = efi_zboot_decompress_slice((u8 *)&elf_header, sizeof(elf_header));
	if (!ret) {
		efi_err("failed to extract header\n");
		return false;
	}

	/* Check the ELF magic */
	if (elf_header.ehdr.e_ident[EI_MAG0] != ELFMAG0 ||
	    elf_header.ehdr.e_ident[EI_MAG1] != ELFMAG1 ||
	    elf_header.ehdr.e_ident[EI_MAG2] != ELFMAG2 ||
	    elf_header.ehdr.e_ident[EI_MAG3] != ELFMAG3) {
		/*
		 * Raw images are padded to the memory size before compression,
		 * so the payload size equals the allocation size.
		 */
		*alloc_size = payload_size;
		*entry = 0;
		return true;
	}

	/*
	 * Check whether the executable header and program headers are laid out
	 * as expected.
	 */
	if (elf_header.ehdr.e_phoff != offsetof(typeof(elf_header), phdr) ||
	    elf_header.ehdr.e_phnum > ARRAY_SIZE(elf_header.phdr)) {
		efi_err("Unexpected ELF header layout\n");
		return false;
	}

	/*
	 * Iterate over the PT_LOAD headers to find the size of the executable
	 * image in memory.
	 */
	for (int i = 0; i < elf_header.ehdr.e_phnum; i++) {
		__auto_type ph = &elf_header.phdr[i];

		if (ph->p_type != PT_LOAD)
			continue;

		min = min(min, ph->p_paddr);
		max = max(max, ph->p_paddr + ph->p_memsz);
	}

	if (min >= max) {
		efi_err("Failed to determine ELF memory size\n");
		return false;
	}

	efi_info("ELF zboot payload detected\n");

	*alloc_size = max - min;
	*entry = elf_header.ehdr.e_entry - elf_header.phdr[0].p_paddr;
	is_elf = true;

	return true;
}

bool efi_zboot_decompress_segments(u8 *out, unsigned long outlen)
{
	efi_memory_attribute_protocol_t *memattr = NULL;
	unsigned long pos = sizeof(elf_header);

	if (!is_elf) {
		/*
		 * If this is a raw image, first copy the data we already
		 * extracted from the compressed blob into the output.
		 */
		memcpy(out, &elf_header, pos);

		return efi_zboot_decompress_slice(out + pos, outlen - pos);
	}

	/* grab a reference to the memory attributes protocol, if available */
	efi_bs_call(locate_protocol, &EFI_MEMORY_ATTRIBUTE_PROTOCOL_GUID, NULL,
		    (void **)&memattr);

	/*
	 * Iterate over the program headers, and decompress the payload of each
	 * PT_LOAD entry. This involves skipping the padding by decompressing
	 * it into the output buffer before overwriting it with the actual
	 * data.
	 */
	for (int i = 0; i < elf_header.ehdr.e_phnum; i++) {
		__auto_type ph = &elf_header.phdr[i];
		void *dst = out + ph->p_paddr - elf_header.phdr[0].p_paddr;
		unsigned long pa = (unsigned long)dst;

		if (ph->p_type != PT_LOAD)
			continue;

		if (ph->p_offset < pos) {
			efi_err("ELF PT_LOAD headers out of order\n");
			return false;
		}

		/* extract and discard the padding */
		while (ph->p_offset > pos) {
			unsigned long l = min(ph->p_offset - pos, ph->p_memsz);

			efi_zboot_decompress_slice(dst, l);
			pos += l;
		}

		/* decompress payload */
		efi_zboot_decompress_slice(dst, ph->p_filesz);

		/* clear area that was not covered by file data */
		if (ph->p_memsz > ph->p_filesz)
			memset(dst + ph->p_filesz, 0, ph->p_memsz - ph->p_filesz);

		if (memattr && ph->p_flags == (PF_R | PF_X)) {
			unsigned long l = ALIGN(ph->p_memsz, EFI_PAGE_SIZE);
			efi_status_t status;

			status = memattr->set_memory_attributes(memattr, pa, l, EFI_MEMORY_RO);
			if (status != EFI_SUCCESS)
				efi_warn("Failed to set EFI_MEMORY_RO on R-X region\n");

			status = memattr->clear_memory_attributes(memattr, pa, l, EFI_MEMORY_XP);
			if (status != EFI_SUCCESS)
				efi_warn("Failed to clear EFI_MEMORY_XP from R-X region\n");
		}

		if (ph->p_flags & PF_X)
			efi_cache_sync_image(pa, ph->p_filesz);

		pos = ph->p_offset + ph->p_filesz;
	}

	return true;
}
