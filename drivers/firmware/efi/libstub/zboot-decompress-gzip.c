// SPDX-License-Identifier: GPL-2.0

#include <linux/efi.h>
#include <linux/zlib.h>

#include <asm/efi.h>

#include "efistub.h"

#include "inftrees.c"
#include "inffast.c"
#include "inflate.c"

extern unsigned char _gzdata_start[], _gzdata_end[];

static struct z_stream_s stream;

efi_status_t efi_zboot_decompress_init(unsigned long *alloc_size,
				       unsigned long *entry)
{
	efi_status_t status;
	int rc;

	/* skip the 10 byte header, assume no recorded filename */
	stream.next_in = _gzdata_start + 10;
	stream.avail_in = _gzdata_end - stream.next_in;

	status = efi_allocate_pages(zlib_inflate_workspacesize(),
				    (unsigned long *)&stream.workspace,
				    ULONG_MAX);
	if (status != EFI_SUCCESS)
		return status;

	rc = zlib_inflateInit2(&stream, -MAX_WBITS);
	if (rc != Z_OK) {
		efi_err("failed to initialize GZIP decompressor: %d\n", rc);
		status = EFI_LOAD_ERROR;
		goto out;
	}

	if (!efi_zboot_check_header(alloc_size, entry)) {
		status = EFI_LOAD_ERROR;
		goto out;
	}

	return EFI_SUCCESS;
out:
	efi_free(zlib_inflate_workspacesize(), (unsigned long)stream.workspace);
	return status;
}

bool efi_zboot_decompress_slice(u8 *out, unsigned long outlen)
{
	int rc;

	stream.next_out = out;
	stream.avail_out = outlen;

	rc = zlib_inflate(&stream, 0);

	return rc == Z_OK || rc == Z_STREAM_END;
}

efi_status_t efi_zboot_decompress(u8 *out, unsigned long outlen)
{
	bool ret = efi_zboot_decompress_segments(out, outlen);

	zlib_inflateEnd(&stream);
	efi_free(zlib_inflate_workspacesize(), (unsigned long)stream.workspace);

	if (!ret) {
		efi_err("GZIP decompression failed\n");
		return EFI_LOAD_ERROR;
	}

	return EFI_SUCCESS;
}
